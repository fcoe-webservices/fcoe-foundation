jQuery.fn.setAllToMaxHeight = function(){
  return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
}

jQuery(function ($) {
  var modalSettings = {
    onOpen: function (dialog) {
          dialog.container.fadeIn(300, function(){
            dialog.container.animate({
              top: '+=10'
            }, 500);
          });
          dialog.data.fadeIn(300);
          dialog.overlay.fadeIn(300);
        },
        onClose: function(dialog) {
          dialog.container.animate({
            top: '+=10'
          }, 500, function () {
            dialog.container.fadeOut(300, function() {
              dialog.overlay.fadeOut(200, function () {
                $.modal.close();
              });
            }); 
          });
        },
     
        opacity: 80,
        overlayCss: {backgroundColor: '#ffffff'},
        overlayClose: true
      };

  $('.click_play').click(function(e) {
    modalSettings.height = '400';
    $(this).next('.video_player').modal(modalSettings);
    e.preventDefault();
  });

  $('.field-name-field-gallery-images .field-item:nth-child(3n+0)').addClass('end');

  $('#play_fcoe_video').click(function(e) {
    modalSettings.height = '480';
    $('#flvplayer').modal(modalSettings);
    e.preventDefault();
  });

  $('.button, blockquote').append('<span class="bg"></span>');
  
  $('.thumbnail:empty').hide().next().css('width','100%');

  $('#nav_footer li:has(ul)').each(function(index) {
    var curNav, newUl;
    curNav = $(this).detach();
    newUl = $('<ul class="secondary"></ul>').append(curNav);
    $('#nav_footer').show().append(newUl);
  });       

  $('.donate_now').bind('click', function(e) {
    $('#donate_now').submit();
    e.preventDefault();
  });

  $('.donate_recurring').bind('click', function(e) {
    $('#donate_recurring').submit();
    e.preventDefault();
  });
  
  $('.click-award').click(function(e) {
    modalSettings.maxWidth = '700';
    modalSettings.height = 'auto';
    $('.read-award').modal(modalSettings);
    e.preventDefault();
  });

});
