<div class="body_wrap">
  <div class="upper_wrap">

    <?php include 'includes/header.inc' ?>

      <div id="main" role="main" class="clearfix">

      <?php print render($title_prefix);?>
        <div id="heading">
          <div class="wrap">
          <div class="kids kids-<?php echo rand(1,6);?>"></div>
            <h2><?php print $title;?></h2>
            <p><?php //fcoe_intro_text($node);?></p>
            <?php $tabs && print render($tabs);?>
          </div>
          <div class="shadow"></div>
        </div><!-- #heading -->

        <div id="content">

          <?php print $messages;?>
          <?php print render($page['content']);?>

        </div><!-- #content -->
      <?php print render($title_suffix); ?>

      <div id="sidebar_first">
        <?php $page['sidebar_first'] && print render($page['sidebar_first']);?>
      </div><!-- #sidebar_first -->

    </div><!-- #main -->

  </div><!-- .upper_wrap -->

  <?php include 'includes/footer.inc' ?>

</div><!-- .body_wrap -->
