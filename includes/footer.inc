 <footer>
    <div class="wrap">
      <a href="#" class="parent_logo">FCOE</a>
      <div id="nav_footer" class="clearfix">
        <?php print render($main_menu_expanded);?>
      </div><!-- #nav_footer -->
      <div id="copyright">
        <p><strong>The Foundation @ FCOE</strong> &copy; 2012</p>
      </div><!-- #copyright -->
    </div><!-- .wrap -->
  </footer>
