    <?php include 'flowplayer.inc';?>
    <header>

      <div class="wrap">
        <h1><a href="/"><?php print $site_name;?></a></h1>

        <?php $page['header'] && print render($page['header']);?>

        <form id="donate_now" action="https://www.paypal.com/cgi-bin/webscr" method="post">
          <input type="hidden" name="cmd" value="_s-xclick">
          <input type="hidden" name="hosted_button_id" value="KFN2TPXTARTAY">
          <!-- <input type="submit" name="donate" value="Donate" class="donate"> -->
        </form>

        <a href="#" class="button donate donate_now">Donate</a>
        <a href="/search" class="search">Search</a>

        <span class="triangle"></span>

      </div><!-- .wrap -->

    </header>
