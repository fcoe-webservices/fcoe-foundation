<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php print $head_title;?></title>

	<meta name="viewport" content="width=device-width">

  <script src="<?php echo base_path(). $directory;?>/js/libs/modernizr-2.5.3.min.js"></script>
  <script src="<?php echo base_path(). $directory;?>/js/libs/flowplayer-3.2.9.min.js"></script>

  <?php print $styles;?>
</head>

<body>
<?php ?>

<?php 
  print $page_top;
  print $page;
  print $scripts;
  print $page_bottom;
?>

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
<!-- <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.2.min.js"><\/script>')</script> -->

<script src="<?php echo base_path(). $directory;?>/js/libs/jquery.simplemodal.1.4.2.min.js"></script>
<script src="<?php echo base_path(). $directory;?>/js/plugins.js"></script>
<script src="<?php echo base_path(). $directory;?>/js/script.js"></script>
<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

</body>
</html>
