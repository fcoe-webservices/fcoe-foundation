<div class="body_wrap front">
  <div class="upper_wrap">

    <?php include 'includes/header.inc'; ?>

    <div id="main" role="main" class="clearfix">
    
      <div id="heading">
        <div class="wrap">
  <?php
  $view = views_get_view('slides');
  $view->set_display('block_1');
  print $view->preview('block_1');
  ?>

        </div><!-- .wrap -->
      </div><!-- #heading -->

      <div id="intro">
        <div class="kids"></div>
        <h2>Excellence, Access &amp; Innovation</h2>
        <p>Advancing excellence, ensuring that young people realize their academic potential, and making a difference in the educational experience of Fresno County students are the fundamental reasons The Foundation @ FCOE was established in 2009. Today, the Foundation is proud to be achieving the promise of its mission: to support the students of Fresno County with exemplary educational programs and services through a shared community responsibility.</p>
        <div class="photo"></div>
      </div>


      <div class="content_wrap">
        <div id="content">
          <div class="tiles"></div>
          <div class="news_items">
  <?php
  $view = views_get_view('news');
  $view->set_display('block_1');
  print $view->preview('block_1');
  ?>
          </div><!-- .news_items -->

        </div><!-- #content -->

        <div class="divider"></div>


        <div class="we_are_fcoe">
          <div class="wrap">
            <div class="left">
              <h3>Who is FCSS?</h3>
              <p>Find out what the Fresno County Office of Education does to support public education.</p>
              <a href="/sites/fcoefoundation.org/files/We_are_FCOE.mp4" id="play_fcoe_video" class="button green">Play Video</a> <a href="http://fcoe.org" class="button blue">FCOE.org</a>
           
            </div><!-- .left -->
            <a href="/about" class="seal">We Are FCOE</a>
          </div><!-- .wrap -->
        </div><!-- .we_are_fcoe -->

      </div><!-- .content_wrap -->

    </div><!-- #main -->

    <?php include 'includes/footer.inc';?>

  </div><!-- .upper_wrap -->

</div><!-- .body_wrap -->
