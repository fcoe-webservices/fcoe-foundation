<?php

/**
 * Allows to print menu in template
 */
function fcoefoundation_preprocess_page(&$variables) {
  $main_menu_tree = menu_tree_all_data('main-menu');
  $variables['main_menu_expanded'] = menu_tree_output($main_menu_tree);
}

