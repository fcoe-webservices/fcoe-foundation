<div class="wrap">
  <div class="photo">
    <?php print $fields['field_donor_photo']->content;?>
  </div>
  <div class="text">
    <?php print $fields['field_donor_quote']->content;?>
    <?php print $fields['title']->content;?>

    <!-- 
    <a href="<?php print $fields['field_donor_video']->content;?>" class="button green click_play">Play Video</a>

     <div id="flvplayer-<?php print $fields['nid']->raw;?>" class="video_player"style="display:none;width:580px;height:400px;"></div>
      <script>
      flowplayer("flvplayer-<?php print $fields['nid']->raw;?>", "/sites/all/themes/fcoefoundation/flash/flowplayer-3.2.10.swf",
         {
          clip: {
            url: "<?php print $fields['field_donor_video']->content;?>",
            scaling: 'orig'
          },
          plugins: {
            controls: {
              url: '/sites/all/themes/fcoefoundation/flash/flowplayer.controls-3.2.10.swf',
              sliderGradient: 'none',
              backgroundColor: '#000000',
               backgroundGradient: 'none',
            }
           },
         });
      </script>
-->
  </div> 

  <div class="views-field-edit-node" style="clear:both;"><?php print $fields['edit_node']->content;?></div>

</div><!-- .wrap -->
